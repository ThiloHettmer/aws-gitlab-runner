# AWS GITLAB-RUNNER

## DESCRIPTION

This project will spin up (default: free tier t2.micro) an aws ec2 instance, installs, registers and starts a 
gitlab-runner.

## REQUIREMENTS

- Terraform
- Ansible
- (optional) Make

## INSTALLATION

- create a file `.env`, copy everything from `.env.example` and fill it out.

## COMMANDS

Please check out the file `Makefile` for all commands.

Here a few:

- `make tinit`: terraform init
- `make tvalidate`: terraform validate
- `make tplan`: terraform plan
- `make tapply`: terraform apply
- `make terraform`: all of the above

## FAQ

<b>[F]Where can I find the keys to connect to my ec2 instance?</b>

[A]All files created by terraform can be found in `tmp/`. These files will not be commited by git.

<b>[F]Is there anything else I have to configure?</b>

[A]Right now only the `.env` file holds all configuration.

## AUTHOR

This project was created by Thilo Hettmer.

Gitlab: https://gitlab.com/ThiloHettmer

## LICENCE

MIT License

Copyright (c) 2021 Thilo Hettmer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
