resource "tls_private_key" "algorithm" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  key_name = var.key_pair
  public_key = tls_private_key.algorithm.public_key_openssh
  create_key_pair = var.create_key_pair

  tags = {
    name = var.tag
  }
}
