resource "local_file" "gitlab_runner_inventory_file" {
  content = templatefile("templates/inventory.tpl", { gitlab_runner_public_dns = aws_instance.gitlab_runner.public_dns })
  filename = "../tmp/inventory.yml"
  file_permission = "600"
  depends_on = [aws_instance.gitlab_runner]
}

resource "local_file" "gitlab_runner_private_key_file" {
  content = tls_private_key.algorithm.private_key_pem
  filename = "../tmp/private_key.pem"
  file_permission = "600"
}

resource "local_file" "gitlab_runner_public_key_file" {
  content = tls_private_key.algorithm.public_key_pem
  filename = "../tmp/public_key.pem"
  file_permission = "600"
}