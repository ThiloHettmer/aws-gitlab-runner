variable "access_key" {
  default = ""
  type = string
}

variable "secret_key" {
  default = ""
  type = string
}

variable "region" {
  default = "eu-central-1"
}

variable "ami" {
  default = "ami-058e6df85cfc7760b"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "tag" {
  default = "gitlab_runner"
}

variable "key_pair" {
  default = "gitlab_runner_key_pair"
}

variable "create_key_pair" {
  default = true
}

variable "security_group_name" {
  default = "gitlab_runner_security_group"
}
