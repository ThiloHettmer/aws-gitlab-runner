include .env

tinit:
	@cd infrastructure && terraform init
tvalidate:
	@cd infrastructure && terraform validate
tplan:
	@cd infrastructure && TF_VAR_access_key=${AWS_ACCESS_KEY} TF_VAR_secret_key=${AWS_SECRET_KEY} terraform plan
tapply:
	@cd infrastructure && TF_VAR_access_key=${AWS_ACCESS_KEY} TF_VAR_secret_key=${AWS_SECRET_KEY} terraform apply -auto-approve
tdestroy:
	@cd infrastructure && TF_VAR_access_key=${AWS_ACCESS_KEY} TF_VAR_secret_key=${AWS_SECRET_KEY} terraform destroy -auto-approve
terraform: tinit tvalidate tplan tapply
ansible:
	@ansible-playbook --private-key=tmp/private_key.pem --inventory=tmp/inventory.yml configuration/playbook.yml --extra-var "GITLAB_REGISTRATION_TOKEN=${GITLAB_REGISTRATION_TOKEN}"
